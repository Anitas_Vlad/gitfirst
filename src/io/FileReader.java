package io;

import publications.*;
import publications.contributor.Contributor;
import publications.enums.Genre;
import publications.enums.Theme;
import publications.exeptions.InvalidBookIdentifier;
import publications.exeptions.InvalidMovieIdentifier;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class FileReader {

    public static ArrayList<Publication> readAndWrite() throws IOException, InvalidMovieIdentifier, InvalidBookIdentifier {

        BufferedReader reader = new BufferedReader(new java.io.FileReader("publications.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("publicationsOut.txt"));

        String line = "";
        while ((line = reader.readLine()) != null) {
//            B~~~Atomic Habits~~~James Clear;Second Author~~~2536134
            String[] splitLine = line.split("~~~");

            //      If Book

            if (splitLine[0].contains("B")) {
                String title = splitLine[1];
                String allAuthors = splitLine[2]; // James Clear;Second Author
                String[] separatedAuthors = allAuthors.split(";");
                //James Clear
                //Second Author
                LinkedHashSet<Contributor> authors = new LinkedHashSet<>();
                for (String separatedAuthor : separatedAuthors) {
                    Contributor a = new Contributor(separatedAuthor);
                    authors.add(a);
                }
                String genre = splitLine[3];
                String ISBN = splitLine[4];
                Book book = new  Book(title, Genre.valueOf(genre), authors, ISBN);
                PublicationStorage.booksStorage.add(book);
            }

            //      If Article

            if (splitLine[0].contains("A")) {
                String title = splitLine[1];
                String allAuthors = splitLine[2];
                String[] separatedAuthors = allAuthors.split(";");
                LinkedHashSet<Contributor> authors = new LinkedHashSet<>();
                for (int i = 0; i < separatedAuthors.length; i++) {
                    Contributor a = new Contributor(separatedAuthors[i]);
                    authors.add(a);
                }
                String theme = splitLine[3];
                String DOI = splitLine[4];
                Article article = new Article(title, Theme.valueOf(theme), authors, DOI);
                PublicationStorage.articlesStorage.add(article);
            }

            //      If Movie

            if (splitLine[0].contains("M")) {
                String title = splitLine[1];
                String allActors = splitLine[2];
                String[] separatedAuthors = allActors.split(";");
                LinkedHashSet<Contributor> actors = new LinkedHashSet<>();
                for (String separatedAuthor : separatedAuthors) {
                    Contributor a = new Contributor(separatedAuthor);
                    actors.add(a);
                }
                String genre = splitLine[3];
                String IMDB = splitLine[4];
                Movie movie = new Movie(title, Genre.valueOf(genre), actors, IMDB);
                PublicationStorage.moviesStorage.add(movie);
            }
        }

        PublicationStorage.articlesStorage.forEach(article -> {
            try {
                writer.write(article + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.write("\n");

        PublicationStorage.booksStorage.forEach(book -> {
            try {
                writer.write(book + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.write("\n");

        for (Movie movie : PublicationStorage.moviesStorage) {
            writer.write(movie + "\n");
        }

        reader.close();
        writer.close();
        return new ArrayList<>();
    }

    public static ArrayList<Publication> query() throws IOException {

        Scanner scan = new Scanner(System.in);

        System.out.println("What brings you out to this library today, milady? ");
        System.out.println("1-Books" + "\n" + "2-Articles" + "\n" + "3-Movies");

        if (scan.nextInt() == 1) {
            System.out.println("Is there a title or author in particular that interests you?");

            PublicationStorage.booksStorage.forEach(book -> {
                String desiredDetail = scan.nextLine();

                if (desiredDetail.equals(book.getTitle()) || String.valueOf(book.getAuthors()).contains(desiredDetail)) {
                    System.out.println("Yes, we have the book right here");

                    // Should return the exact same book
                }
            });
        }

        return new ArrayList<>();
    }

}
