import io.FileReader;
import publications.Book;
import publications.Movie;
import publications.contributor.Contributor;
import publications.enums.Genre;
import publications.exeptions.InvalidBookIdentifier;
import publications.exeptions.InvalidMovieIdentifier;

import java.io.IOException;
import java.util.LinkedHashSet;

public class Main {
    public static void main(String[] args) throws InvalidMovieIdentifier, InvalidBookIdentifier, IOException {


        FileReader.readAndWrite();
        FileReader.query();

//        Contributor vlad = new Contributor("Vlad");
//        LinkedHashSet<Contributor> contributors = new LinkedHashSet<>();
//        contributors.add(vlad);
//        Movie matrix = new Movie("Matrix", Genre.FANTASY, contributors, "123");
//        Book book = new Book("book", Genre.ART, contributors, "978-3-16-148410-0");

    }
}
