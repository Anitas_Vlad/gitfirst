package collections;

import java.util.Comparator;
import java.util.Objects;

public class Person {

    String name;
    int age;
    char genre;


    public Person(String name, int age, char genre) {
        this.name = name;
        this.age = age;
        this.genre = genre;
    }
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    public void setAge(int num) {
        this.age += num;
    }

    public int getAge() {
        return age;
    }

    public char getGenre() {
        return genre;
    }
    public String getName(){
        return name;
    }

    @Override
    public String toString() {
        return "Person" +
                " " + name +
                " " + age ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) && Objects.equals(age, person.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }


}
