package collections;

import java.util.Comparator;

public class CustomComparator implements Comparator<String> {
    /**
     * @param o1
     * @param o2
     * @return -1 if o1 < o2 /// 0 if o1 == o2 /// 1 if o1 > o2
     */

    @Override
    public int compare(String o1, String o2) {
        Integer i1 = null;
        Integer i2 = null;
        try {
            i1 = Integer.parseInt(o1);
            i2 = Integer.parseInt(o2);

        } catch (NumberFormatException e) {

        }
        //   Both are null
        if (i1 == null && i2 == null) {
            if (o1.compareTo(o2) < 0) {
                // o1 < o2
                return -1;

            } else if (o1.compareTo(o2) == 0) {
                return 0;

            } else if (o1.compareTo(o2) > 0) {
                return 1;
            }
        }

        //Only one of them is a number
        if (i1 != null && i2 == null){
            // o1 e număr pentru că a putut fi convertit la i1          (nr)
            // o2 nu e număr pentru că nu a putut fi convertit la i2    (string)
            // o2, o1    (deci o1 e mai mare)
        }

        //   Both are numbers
        return 0;
    }
}
//prima data numerele === apoi cifrele