package collections;

import java.util.Comparator;

public class AgePersonComparator implements Comparator<Person> {

    // < = >
    // - 0 +
    @Override
    public int compare(Person o1, Person o2) {
        if (o1.age == o2.age && o1.name == o2.name) {
            return 0;
        } else if (o1.age < o2.age) {
            return -1;
        } else return 1;
    }
}
