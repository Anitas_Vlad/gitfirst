package collections;

import publications.Book;

import java.util.Comparator;

public class BookComparator implements Comparator<Book> {


    @Override
    public int compare(Book o1, Book o2) { // In alphabetical order
        return o1.getIdentifier().compareTo(o2.getIdentifier())* (-1);
    }
}
