package collections;

import java.util.Comparator;

public class AlphabeticalPersonComparator implements Comparator<Person> {

    /**
     *
     *  if (o1.name == o2.name) {
     *             return 0;
     *         } else if (o1.name.compareTo(o2.name) < 0) {
     *             return 1;
     *         } else return -1;
     *
     */

        @Override
    public int compare(Person o1, Person o2) {
        return o1.name.compareTo(o2.name) * (-1);
    }
}
