package collections;

import publications.Book;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        HashSet<Integer> IntegerHashSet = new HashSet<>();
        Integer int1 = 693;
        Integer int2 = 5;
        Integer int3 = 38;
        Integer int4 = 323;
        Integer int5 = 5;
        IntegerHashSet.add(int1);
        IntegerHashSet.add(int2);
        IntegerHashSet.add(int3);
        IntegerHashSet.add(int4);
        IntegerHashSet.add(int5);
        IntegerHashSet.forEach(System.out::println);
        System.out.println("Contains 4? " + IntegerHashSet.contains(4));
        System.out.println();

        TreeSet<Integer> integerTreeSet = new TreeSet<>(); // by default, java's gonna increase-order them
        integerTreeSet.add(7);
        integerTreeSet.add(2);
        integerTreeSet.add(3);
        integerTreeSet.add(1);
        integerTreeSet.add(5);
        integerTreeSet.forEach(System.out::println);
        System.out.println();


        TreeSet<Integer> integerTreeSetWithOurComparator = new TreeSet<>(Comparator.reverseOrder()); // ours will make it in reverse
        integerTreeSetWithOurComparator.add(7);
        integerTreeSetWithOurComparator.add(2);
        integerTreeSetWithOurComparator.add(3);
        integerTreeSetWithOurComparator.add(1);
        integerTreeSetWithOurComparator.add(5);
        integerTreeSetWithOurComparator.forEach(System.out::println);
        System.out.println();

        TreeSet<String> stringTreeSet = new TreeSet<>();
        stringTreeSet.add("mama");
        stringTreeSet.add("tata");
        stringTreeSet.add("3");
        stringTreeSet.add("21");
        stringTreeSet.add("1mama");
        stringTreeSet.forEach(System.out::println);
        System.out.println();

        TreeSet<String> stringTreeSet1 = new TreeSet<>(Comparator.reverseOrder());
        stringTreeSet1.add("mama");
        stringTreeSet1.add("tata");
        stringTreeSet1.add("3");
        stringTreeSet1.add("21");
        stringTreeSet1.add("1mama");
        stringTreeSet1.forEach(System.out::println);


        //   TreeSet<Book> books = new TreeSet<>(Comparator.)
        Book book1 = new Book("Book1");
        Book book2 = new Book("Book2");
        Book book3 = new Book("Book3");
        Book book4 = new Book("Book4");
        Book book5 = new Book("Book5");


        Person p1 = new Person("Ion", 2, 'M');
//        Person p1 = new Person("Ion", 2, 'M');
//        Person p1 = new Person("Ion", 2, 'M');
        Person p2 = new Person("Maria", 4, 'F');
        Person p3 = new Person("Geroge", 2, 'M');
        Person p4 = new Person("Ana", 26, 'F');
        Person p5 = new Person("Mihai", 2, 'M');
        Person p6 = new Person("Gabriel", 32, 'M');
        Person p7 = new Person("Ion", 20, 'M');
        Person p8 = new Person("Ion", 2, 'M');
        Person p9 = new Person("Ion", 2, 'M');
        Person p10 = new Person("Ion", 2, 'M');
        Person p11 = new Person("Ion", 2, 'M');
        Person p12 = new Person("Ion", 2, 'M');

        TreeSet<Person> people = new TreeSet<Person>(new AgePersonComparator());

        people.add(p1);
        people.add(p8);
        people.add(p9);
        people.add(p10);
        people.add(p11);
        people.add(p12);
        people.add(p2);
        people.add(p3);
        people.add(p4);
        people.add(p5);
        people.add(p6);
        people.add(p7);


        //-------------------------------
//        List<Person> under10 = people.stream().filter(pips -> {
//            if (pips.getAge() <= 10) {
//                return true;
//            }
//            return false;
//        }).collect(Collectors.toList());
        //-------------------------------

        // lambda function
        // (parametri) -> {corpul functiei}
        List<Person> majori = people.stream().filter((pers) -> {
            if (pers.getAge() > 18) {
                return true;
            } else return false;
        }).collect(Collectors.toList());

        System.out.println("Majori:");
        majori.forEach(System.out::println);
        System.out.println();

        List<Person> male = people.stream().filter(person -> {
            if (person.getGenre() == 'm') {
                return true;
            } else return false;
        }).collect(Collectors.toList());
        male.forEach(System.out::println);

        System.out.println("under10e");
        List<Person> under10 = people.stream().filter(pips -> {
            if (pips.getAge() <= 10) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        under10.forEach(System.out::println);
    }
}
