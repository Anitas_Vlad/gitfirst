package generics.gift;
/**
 * @Param <C> type of the card in the gift
 * @Param <G> type of the actual gift
 */

import generics.card.Card;

public class Gift<C,G>{
    C card;
    G gift;

    public Gift(C card, G gift) {
        this.card = card;
        this.gift = gift;
    }

    public C getCard() {
        return card;
    }

    public void setCard(C card) {
        this.card = card;
    }

    public G getGift() {
        return gift;
    }

    public void setGift(G gift) {
        this.gift = gift;
    }
}
