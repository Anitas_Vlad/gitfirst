package generics.gift;

import generics.card.DigitalCard;

public class CorporateGift<C extends DigitalCard>extends Gift<C, Integer> {

    public CorporateGift(C card, Integer gift){
        super(card, gift);
    }
}
