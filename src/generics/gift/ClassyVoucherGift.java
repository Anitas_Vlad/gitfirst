package generics.gift;

import generics.Box;
import generics.gift.Gift;
import generics.card.PaperCard;

public class ClassyVoucherGift extends Gift<PaperCard, Box<String>> {
    public ClassyVoucherGift(PaperCard card, Box<String> gift){
        super(card, gift);
    }
}
