package generics.gift;

import generics.Box;
import generics.gift.Gift;
import generics.card.PaperCard;

public class ClassyMoneyGift extends Gift<PaperCard, Box<Integer>> {
    public ClassyMoneyGift(PaperCard card, Box<Integer>gift){
        super(card, gift);
    }
}
