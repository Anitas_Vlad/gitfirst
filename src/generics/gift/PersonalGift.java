package generics.gift;

import generics.Box;
import generics.card.FizicalCard;
import generics.gift.Gift;

public class PersonalGift<C extends FizicalCard, T> extends Gift<C, Box<T>> {
    public PersonalGift(C card, Box<T> gift){
        super(card, gift);
    }

    /**
     * Sau
     *
     * public class PersonalGift <T> extends Gift<PhisicalCard, Box<T>>{
     *     public PersonalGift(PhisicalCard card, Box<T> gift){
     *         super(card, gift);
     *     }
     * }
     */

}
