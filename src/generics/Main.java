package generics;

import generics.card.*;
import generics.gift.*;

public class Main {
    public static void main(String[] args) {
        Box<Integer> boxOfInteger = new Box();
        boxOfInteger.setItem(12);

        Box<String> voucher20$ = new Box<>();
        voucher20$.setItem("20$");

        Box<String> boxOfString = new Box<>();
        boxOfString.setItem("String Box");

        Box<Apple> boxOfApples = new Box<>();
        boxOfApples.setItem(new Apple("green", 20));

        Package<Integer> integerPackage = new Package<>();
        integerPackage.setItem(20);
        integerPackage.setDeliveryAddress("toYou");

        Package<Apple> applePackage = new Package<>();
        applePackage.setItem(new Apple("red", 10));
        applePackage.setDeliveryAddress("Outta Here");

        Gift<DigitalCard, Integer> moneythroughDigitalCard = new Gift<>(new DigitalCard(), 850);
        Gift<Email, String> emailImportant = new Gift<>(new Email(), "Ia hai du-te ca deja e pre de tot");

        ClassyMoneyGift classyMoneyGift = new ClassyMoneyGift(new PaperCard(), boxOfInteger);
        ClassyVoucherGift voucherGift = new ClassyVoucherGift(new PaperCard(), voucher20$);

        BoxWithMultipleItems<Gift> boxWithMultipleGifts = new BoxWithMultipleItems<>();
        boxWithMultipleGifts.setAddress("Home");

        boxWithMultipleGifts.addItem(moneythroughDigitalCard);
        boxWithMultipleGifts.addItem(emailImportant);
        boxWithMultipleGifts.addItem(classyMoneyGift);
        boxWithMultipleGifts.addItem(voucherGift);
        boxWithMultipleGifts.addItem(moneythroughDigitalCard);

        System.out.println(boxWithMultipleGifts.isReadyToShip());

        Email vladEmail = new Email();
        CorporateGift corporateGift = new CorporateGift(vladEmail, 100);

        PaperCard vladPaperCard = new PaperCard();
        Box<String> tigerBox = new Box<>();
        tigerBox.setItem("Tiger");
        PersonalGift<FizicalCard, String> personalGift = new PersonalGift<>(vladPaperCard, tigerBox);


    }
}
