package generics;

import java.util.ArrayList;

public class BoxWithMultipleItems<T> {
    ArrayList<T> items;
    String address;

    public BoxWithMultipleItems() {
        items = new ArrayList<>();
    }

    public void setItems(ArrayList<T> items) {
        this.items = items;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void addItem(T item) {
        this.items.add(item);
    }

    public void displayItems() {
        this.items.forEach(System.out::println);
    }

    public BoxWithMultipleItems(ArrayList<T> items) {
        this.items = items;
    }

    //vrem sa facem o metoda prin care sa luam giftul din pozitia x dintr un arrayList !!!
    public void display(Integer position) {
        System.out.println("Display the " + position + " element.");
        System.out.println(items.get(position - 1));
    }

    public boolean emptyBox() {
        return items.size() == 0;
    }

    public boolean isReadyToShip() {
        return items.size() >= 5 && this.address != null;
    }


}
