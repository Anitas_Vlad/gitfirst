package generics;

public class Package<T> {
    T item;
    String deliveryAddress;

    public void setItem(T item){
        this.item = item;
    }

    public T getItem(){
        return item;
    }

    public void setDeliveryAddress(String deliveryAddress){
        this.deliveryAddress= deliveryAddress;
    }

    public String getDeliveryAddress(){
        return deliveryAddress;
    }

}
