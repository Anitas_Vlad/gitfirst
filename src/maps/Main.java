package maps;

import collections.Person;
import com.sun.source.tree.Tree;
import publications.Movie;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();

//        <1, "unu">
//        <2, "doi">
//        <K, V>
        Map<Integer, String> numbers = new HashMap<>();
        Map.Entry<Integer, String> pairFromMap;
//        add<- lasa programul sa decida unde/cum adauga

        numbers.put(1, "unu");
        numbers.put(2, "doi");
        numbers.put(1, "unu");
        numbers.put(3, "doi");
        numbers.put(4, "doi");

        Scanner scan = new Scanner(System.in);
        String userInput = scan.nextLine();

        for (Map.Entry<Integer, String>elementFromMap: numbers.entrySet()){
            //Daca value din elementFromMap este egal cu input-ul de la user
            if (elementFromMap.getValue().equals(userInput)){
                System.out.println("The Key for your element is: " + elementFromMap.getKey());
            }
        }

        for(Map.Entry<Integer, String> e : numbers.entrySet()){
            System.out.println(e.getKey() + " " + e.getValue());
        }

        for (Integer i:numbers.keySet()){
            System.out.println(i);
        }

        for (String i : numbers.values()){
            System.out.println(i);
        }
        System.out.println();

        Set<String> treeSet = new TreeSet<>();
        treeSet.add("ala");
        treeSet.add("bala");
        treeSet.add("portocala");
        treeSet.add("ala");

        for (String i: treeSet){
            System.out.println(i);
        }

        //      <K, Value>
        //      <2, doi>
        //      doi
        System.out.println(numbers.get(2)); // ^ ne va da valoarea.
        System.out.println(numbers.containsKey(3));

        /**
         * Every person chooses a fav movie. A movie can be more people's fav.
         *
         * // Oana --- Romania Salbatica
         * // Iulia --- Romania Salbatica
         */
        Map<Person, Movie> favoriteMovies;

        /**
         * Biblioteca : putem inchiria filme. O persoana poate inchiria mai multe filme.
         */


        Map<Movie, Person> rentedMovies;

    }
}
