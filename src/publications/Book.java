package publications;
import publications.contributor.Contributor;
import publications.enums.Genre;
import publications.exeptions.InvalidBookIdentifier;

import java.util.LinkedHashSet;

public class Book extends Publication {

    private LinkedHashSet<Contributor> authors;
    private Genre genre;
    private String ISBN;

    public Book(String title){
        super(title);
    }
    public Book(String title, Genre genre, LinkedHashSet<Contributor> authors, String ISBN) throws InvalidBookIdentifier {
        super(title);
        if (!this.ISBN.replace("-","").matches("[0-9]{13}")){
            throw new InvalidBookIdentifier("Invalid Book Identifier. " + ISBN + " contains illegal characters" );
        }
        this.genre = genre;
        this.authors = authors;
        this.ISBN = ISBN;
    }

    public LinkedHashSet<Contributor> getAuthors() {
        return authors;
    }

    @Override
    public String getIdentifier() {
        return ISBN;
    }

    @Override
    public String toString() {
        return "Book: " + getTitle() +
                " - " + authors +
                " - " + genre +
                " - " + ISBN ;
    }

//    public Contributor get(Integer position){
//        return authors.
//    }
}
