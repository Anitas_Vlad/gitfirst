package publications.enums;

public enum Genre {
    FANTASY, SELF_HELP, SCIENCE, COMPUTER_SCIENCE, HORROR, ROMANCE,ART;
}
