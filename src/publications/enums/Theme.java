package publications.enums;

public enum Theme {
    COMPUTER_SCIENCE, BIOLOGY, PSYCHOLOGY;
}
