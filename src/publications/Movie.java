package publications;

import publications.contributor.Contributor;
import publications.enums.Genre;
import publications.exeptions.InvalidMovieIdentifier;

import java.util.LinkedHashSet;

public class Movie extends Publication {

    private Genre genre;
    private LinkedHashSet<Contributor> actors;
    private String IMDB;

    public Movie(String title, Genre genre, LinkedHashSet<Contributor> actors, String IMDB) throws InvalidMovieIdentifier {
        super(title);
        this.genre = genre;
        this.actors = actors;
        this.IMDB = IMDB;
        if (!IMDB.matches("[a-zA-Z0-9]+")) {
            throw new InvalidMovieIdentifier("The provided IMDB id " + IMDB + " contains invalid characters");
        }
    }

    @Override
    public String getIdentifier() {
        return IMDB;
    }

    @Override
    public String toString() {
        return "Movie: " + getTitle() +
                " - " + genre +
                " - " + actors +
                " - " + IMDB;
    }
}
