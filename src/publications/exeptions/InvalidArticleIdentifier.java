package publications.exeptions;

public class InvalidArticleIdentifier extends RuntimeException{

    public InvalidArticleIdentifier(String message){
        super(message);
    }
}
