package publications.exeptions;

public class InvalidBookIdentifier extends Exception{
    public InvalidBookIdentifier(String message){
        super(message);
    }
}
