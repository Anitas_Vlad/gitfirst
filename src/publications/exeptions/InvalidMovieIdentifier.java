package publications.exeptions;

public class InvalidMovieIdentifier extends Exception {

    public InvalidMovieIdentifier(String message){
        super(message);
    }
}
