package publications;
import publications.contributor.Contributor;
import publications.enums.Theme;
import publications.exeptions.InvalidArticleIdentifier;

import java.util.LinkedHashSet;

public class Article extends Publication {

    private String DOI;
    private Theme theme;
    private LinkedHashSet<Contributor> authors;

    public Article(String title, Theme theme, LinkedHashSet<Contributor> authors, String DOI) throws InvalidArticleIdentifier{
        super(title);
        this.theme = theme;
        this.authors = authors;
        this.DOI = DOI;
        if (!this.DOI.matches("")){

        }
    }

    public String getIdentifier() {
        return DOI;
    }

    public String toString() {
        return "Article: " +
                " - " + DOI + '\'' +
                " - " + theme +
                " - " + authors +
                '}';
    }
}
