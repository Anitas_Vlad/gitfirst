package streams;

import collections.AgePersonComparator;
import collections.Person;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        ArrayList<Person> people = new ArrayList<>();

        people.add(new Person("Maria", 20, 'F'));
        people.add(new Person("Ionel", 26, 'M'));
        people.add(new Person("Ionel", 26, 'M'));
        people.add(new Person("Apapa", 10, 'M'));
        people.add(new Person("Ekaga", 27, 'F'));

        System.out.println("forEach");
        people.forEach(p-> System.out.println(p));

        System.out.println("toStream().forEach");
        //      1 parametru
        //      nu returneaza nimic
        people.stream().forEach(p -> System.out.println(p));

        //      1 parametru
        //      return boolean
        //      best practice : do not modify the object
        System.out.println();
        people.stream()
                .filter(p-> p.getAge()<= 18)
                .filter(p-> p.getGenre() == 'M')
                .forEach(System.out::println);

        // 1 parametru
        // retrun orice tip
        people.stream().map(p -> {
            return "Hello, " + p.getName();
        }).forEach(s -> System.out.println(s));

        System.out.println();
        System.out.println("Distinct");
        people.stream()
                .distinct()
                .forEach(System.out::println);

        List<String> stringList = Arrays.asList("S","a", "l", "u", "t");
        //identify
        //lamba takes 2 elements : total , current
        String result = stringList.stream().reduce("Hey,", (total, current)->{
            System.out.println("Current right now: " + current);
            System.out.println("Total right now is:" + total);
            return total + current;
        });
        System.out.println("result of reduce: " + result);

        List<Integer> integers = Arrays.asList(1, 2, 3, -3, 4, 5, 6, 7);
        Integer sum = integers.stream().reduce(0, (total, current)->{
            return total + current;
        });
        System.out.println("Result of sum is: " + sum);

        Integer product = integers.stream().reduce(1, (total, current)-> {
            return total * current;
        });
        System.out.println("Result of product is: " + product );

        Integer min = integers.stream().min(Comparator.naturalOrder()).get();
        System.out.println("Result of min: " + min);

//        Integer max = integers.stream().max(Comparator.reverseOrder()).get();
//        System.out.println(max);

        // metoda care are UN SINGUR parametru si (metoda e statica SAU metoda e o metoda a obiectului)
        integers.stream().limit(4).forEach(System.out::println);

        people.stream().forEach(System.out::println);

//        people.stream().forEach(p -> p.introducePerson());
        people.stream().distinct().forEach(Main::introducePerson);

    }

    public static void introducePerson(Person p){
        System.out.println("Hello! This is my friend, " + p.getName());
    }
}
