package anonymousClasses;

import collections.AgePersonComparator;
import collections.Person;

import java.util.Comparator;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        TreeSet<Person> people = new TreeSet<>(new AgePersonComparator());

        people.add(new Person("Maria", 20));
        people.add(new Person("Ionel", 26));
        people.add(new Person("Apapa", 10));
        people.add(new Person("Ekaga", 27));

        for (Person p : people) {
            System.out.println(p);
        }

        TreeSet<Person> people1 = new TreeSet<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                if(o1.getAge() == o2.getAge() && o1.getName() == o2.getName()) {
                    return 0;
                } else if (o1.getAge() < o2.getAge()) {
                    return -1;
                } else return 1;
            }
        });

        people1.add(new Person("Maria", 20));
        people1.add(new Person("Ioana", 24));
        people1.add(new Person("Vasile", 18));
        people1.add(new Person("George", 16));

        for(Person p : people1) {
            System.out.println(p);
        }


        TreeSet<Integer> integers = new TreeSet<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 == o2) {
                    return 0;
                }
                if (o1 < o2) {
                    return 1;
                } else return -1;
            }
        });
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(5);
        integers.add(6);
        for (Integer i : integers) {
            System.out.println(i);
        }

        TreeSet<Integer> integers1 = new TreeSet<>((o1, o2) -> {
            if (o1 == o2) {
                return 0;
            }
            if (o1 < o2) {
                return 1;
            } else return -1;
        });
        for (Integer itr : integers1){
            System.out.println(itr);
        }

        // TreeSet of Strings and the comparator should sort them by length --->>>   SORT BY LENGTH

        TreeSet<String> strings = new TreeSet<>((s1, s2) -> {
            if (s1.length() == s2.length()) {
                return 0;
            }
            if (s1.length() < s2.length()) {
                return -1;
            } else return 1;
        });
        strings.add("Ana");
        strings.add("George");
        strings.add("Bunica");
        strings.add("Maricel");
        strings.add("Thor");

        strings.forEach(s -> System.out.println(s));
    }
}
